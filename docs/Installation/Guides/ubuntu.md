# Install on Ubuntu 18.04 / 20.04 / 22.04 LTS

## System requirements

### Server

**Server Software Requirements:**

- Operating system:
  - Ubuntu 18.04 / 20.04 / 22.04 LTS
- DBMS:
  - MariaDB 10.5 and higher
- Python: 3.10.8

---

### 1. Update the machine

First, let's make sure the machine is up to date.

```bash
sudo apt-get update && sudo apt-get upgrade && sudo apt-get dist-upgrade
```

### 2. Installing Python 3.10.8

...

### 3. Install the requirements
First of all create and activate venv

```bash
python3.10 -v venv venv
```

```bash
source ./venv/bin/activate
```

Install MariaDB Connector
```bash
wget https://downloads.mariadb.com/MariaDB/mariadb_repo_setup
```

```bash
chmod +x mariadb_repo_setup
```

```bash
# use your ubuntu version: bionic, focal or jammy
sudo ./mariadb_repo_setup --mariadb-server-version="mariadb-10.6" --os-type=ubuntu --os-version="focal" 
```
Finally, install the requirements
```bash
pip install -r requirements.txt
```

---
> **! Documentation in development**

