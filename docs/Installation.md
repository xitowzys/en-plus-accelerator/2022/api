# Installation

## Guides

- [Install on Ubuntu 18.04 / 20.04 / 22.04 LTS](Installation/Guides/ubuntu.md)

## By Platform

- [Docker](Installation/By-Platform/docker.md)