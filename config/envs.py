from typing import Any
from environs import Env

env: Env = Env()
env.read_env()


ENVS: dict[str, Any] = {}

ENVS["DB_HOST"] = env.str("DB_HOST", "127.0.0.1")
ENVS["DB_PORT"] = env.str("DB_PORT", "3306")
ENVS["DB_DATABASE"] = env.str("DB_DATABASE")
ENVS["DB_USERNAME"] = env.str("DB_USERNAME")
ENVS["DB_PASSWORD"] = env.str("DB_PASSWORD")

ENVS["JWT_SECRETS"] = env.str("JWT_SECRETS")
ENVS["JWT_ALGORITHM"] = env.str("JWT_ALGORITHM", "HS256")

ENVS["CODE_SIGN_UP_SALT"] = env.str("CODE_SIGN_UP_SALT")
