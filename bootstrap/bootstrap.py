from modules import loguru_init

from core import di, api


def bootstrap() -> None:
    loguru_init.logger_configuration()

    di.DomainContainer().init_database_tables().execute()

    _api: api.API = api.API()
    _api.run_uvicorn_server()
