from dependency_injector import containers, providers
from dependency_injector.providers import Singleton

from core.data.repository import *
from core.data.storage.database import *


class DataContainer(containers.DeclarativeContainer):

    config = providers.Configuration()

    database_init_storage: Singleton[DatabaseInitTables] = providers.Singleton(
        DatabaseInitTables
    )

    person_database: Singleton[PersonDatabase] = providers.Singleton(
        PersonDatabase
    )

    auth_codes: Singleton[AuthCodes] = providers.Singleton(
        AuthCodes
    )

    init_database_repository_impl: Singleton[IInitDBRepositoryImpl] = providers.Singleton(
        IInitDBRepositoryImpl,
        init_db_storage=database_init_storage
    )

    person_database_repository_impl: Singleton[PersonDatabaseRepositoryImpl] = providers.Singleton(
        PersonDatabaseRepositoryImpl,
        person_database_storage=person_database
    )

    auth_codes_database_repository_impl: Singleton[AuthCodesDatabaseRepositoryImpl] = providers.Singleton(
        AuthCodesDatabaseRepositoryImpl,
        auth_codes_database_storage=auth_codes
    )
