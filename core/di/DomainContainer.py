from dependency_injector import containers, providers
from dependency_injector.providers import Singleton, Container

from core.domain.usecases import *
from .DataContainer import DataContainer


class DomainContainer(containers.DeclarativeContainer):

    dataContainer: Container[DataContainer] = providers.Container(
        DataContainer)

    init_database_tables: Singleton[InitDatabaseTablesUsecase] = providers.Singleton(
        InitDatabaseTablesUsecase,
        init_db_repository=DataContainer.init_database_repository_impl
    )

    add_person_in_database: Singleton[AddPersonInDatabaseUsecase] = providers.Singleton(
        AddPersonInDatabaseUsecase,
        person_database_repository=DataContainer.person_database_repository_impl
    )

    get_id_person: Singleton[GetIdPersonUsecase] = providers.Singleton(
        GetIdPersonUsecase,
        person_database_repository=DataContainer.person_database_repository_impl
    )

    code_generation_for_sign_up: Singleton[CodeGenerationForSignUpPersonUsecase] = providers.Singleton(
        CodeGenerationForSignUpPersonUsecase,
        person_database_repository=DataContainer.person_database_repository_impl
    )

    person_authentication: Singleton[PersonAuthenticationUsecase] = providers.Singleton(
        PersonAuthenticationUsecase,
        auth_codes_database_repository=DataContainer.auth_codes_database_repository_impl)
