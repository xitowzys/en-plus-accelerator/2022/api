tags_metadata: list[dict[str, str]] = [
    {
        "name": "Testing API",
        "description": "To check the API workability",
    },
    {
        "name": "Code for sign up",
        "description": "Requests for registration codes for persons",
    },
    {
        "name": "Person",
        "description": "Requests for person management",
    },
    {
        "name": "Authentication",
        "description": "Requests for person authentication",
    },
]
