from pydantic import BaseModel, SecretStr


class PersonAuthentication200(BaseModel):
    data: str


class PersonAuthentication(BaseModel):
    code: SecretStr
    telegram_id: int
