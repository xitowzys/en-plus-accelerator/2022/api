from dataclasses import Field
from typing import Union
from pydantic import BaseModel, Field
from fastapi import Query
from typing import Optional


class GetIdPerson(BaseModel):
    first_name: str = Query(...)
    last_name: str = Query(...)
    middle_name: Union[str, None] = Query(default=None)


class GetIdPersonCode200(BaseModel):
    id: int = Field(alias="id")
    first_name: str = Field(alias='firstName')
    last_name: str = Field(alias='lastName')
    middle_name: Union[str, None] = Field(alias='middleName')
    role: str = Field(alias='role')
    position: str = Field(alias='position')


class CodeGeneratePerson(BaseModel):
    person_id: int


class CodeGeneratePerson200(BaseModel):
    data: str
    code: str


class AddPerson(BaseModel):
    first_name: str = Field(alias='firstName')
    last_name: str = Field(alias='lastName')
    middle_name: Union[str, None] = Field(alias='middleName')
    role: str = Field(alias='role')
    position: str = Field(alias='position')

    class Config:
        schema_extra: dict[str, dict[str, str]] = {
            "example": {
                "firstName": "Тимофей",
                "lastName": "Мартынов",
                "middleName": "Вениаминович",
                "role": "Работник",
                "position": "Сварщик",
            }
        }


class AddPersonCode200(BaseModel):
    data: str


class AddPersonCode500(BaseModel):
    class err(BaseModel):
        code: int
        err_msg: str

    detail: err
