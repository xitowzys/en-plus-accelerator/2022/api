from fastapi import FastAPI
from loguru import logger
from uvicorn_loguru_integration import run_uvicorn_loguru
import uvicorn

from .v1.routes import hello_world, persons, code_for_sign_up, person_authentication
from fastapi_versioning import VersionedFastAPI

from .tags import tags_metadata


class API:
    def __init__(self) -> None:
        self.__app: FastAPI = FastAPI(
            title="Labor protection API"
        )
        self.__init_routes()

    def __init_routes(self) -> None:
        logger.success("Routers initialized")

        self.__app.include_router(hello_world.router)
        self.__app.include_router(persons.router)
        self.__app.include_router(code_for_sign_up.router)
        self.__app.include_router(person_authentication.router)

    def run_uvicorn_server(self) -> None:

        self.__app = VersionedFastAPI(
            self.__app,
            version_format='{major}',
            prefix_format='/v{major}',
            openapi_tags=tags_metadata
        )

        run_uvicorn_loguru(
            uvicorn.Config(
                app=self.__app,
                host="127.0.0.1",
                port=8080,
                log_level="info"
            )
        )
