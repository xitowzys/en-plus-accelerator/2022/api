from fastapi import APIRouter
from fastapi_versioning import version

router: APIRouter = APIRouter()


@router.get("/hello-world", tags=["Testing API"])
@version(1)
async def get_hello_world() -> dict[str, str]:
    return {"data": "Hello World"}
