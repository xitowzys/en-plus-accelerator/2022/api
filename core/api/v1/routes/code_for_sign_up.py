from pprint import pprint
from fastapi.exceptions import HTTPException
from fastapi import APIRouter, Body, Depends
from fastapi_versioning import version
from typing import List

from ...models.Person import CodeGeneratePerson, CodeGeneratePerson200

from core.di import DomainContainer
from core.domain.models import Person as domain_model_person
from core.domain.usecases import CodeGenerationForSignUpPersonUsecase

from loguru import logger

router: APIRouter = APIRouter()

GLOBAL_TAG: str = "Code for sign up"


@router.post("/code-sign-up", tags=[GLOBAL_TAG])
@version(1)
async def code_generation_for_sign_up(person: CodeGeneratePerson = Body(...)) -> CodeGeneratePerson200:
    code_generation_for_sign_up: CodeGenerationForSignUpPersonUsecase = DomainContainer(
    ).code_generation_for_sign_up()

    code_generation_for_sign_up_domain: domain_model_person.CodeGeneratePerson = domain_model_person.CodeGeneratePerson(
        person_id=person.person_id
    )

    result, err = code_generation_for_sign_up.execute(
        code_generate_person=code_generation_for_sign_up_domain)

    if not err is None:
        raise HTTPException(status_code=400, detail={
                            "code": err["code"], "err_msg": err["msg"]})

    if result is not None:
        return CodeGeneratePerson200(
            data="The registration code has been created",
            code=result
        )
    else:
        # TODO: Change the code
        return CodeGeneratePerson200(
            data="The registration code has been created",
            code="None"
        )


@router.get("/code-sign-up", tags=[GLOBAL_TAG], deprecated=True)
@version(1)
async def get_code_for_sign_up() -> None:
    pass


@router.put("/code-sign-up", tags=[GLOBAL_TAG], deprecated=True)
@version(1)
async def change_code_for_sign_up() -> None:
    pass


@router.delete("/code-sign-up", tags=[GLOBAL_TAG], deprecated=True)
@version(1)
async def delete_code_for_sign_up() -> None:
    pass
