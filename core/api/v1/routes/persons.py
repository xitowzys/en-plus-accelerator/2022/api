from pprint import pprint
from fastapi.exceptions import HTTPException
from fastapi import APIRouter, Body, Depends
from fastapi_versioning import version
from typing import List

from ...models.Person import AddPerson, AddPersonCode200, AddPersonCode500, CodeGeneratePerson, GetIdPerson, GetIdPersonCode200

from core.di import DomainContainer
from core.domain.models import Person as domain_model_person
from core.domain.usecases import AddPersonInDatabaseUsecase, GetIdPersonUsecase, CodeGenerationForSignUpPersonUsecase

from loguru import logger


router: APIRouter = APIRouter()


@router.post(
    "/person",
    tags=["Person"],
    responses={200: {"model": AddPersonCode200},
               500: {"model": AddPersonCode500, "description": "Return an error when query to the database"}},
)
@version(1)
async def add_person(person: AddPerson = Body(...)) -> dict[str, str]:

    person_domain: domain_model_person.AddPerson = domain_model_person.AddPerson(
        first_name=person.first_name,
        last_name=person.last_name,
        middle_name=person.middle_name,
        role=person.role,
        position=person.position
    )

    add_person_in_database: AddPersonInDatabaseUsecase = DomainContainer(
    ).add_person_in_database()

    _, err = add_person_in_database.execute(person=person_domain)

    if not err is None:
        raise HTTPException(status_code=400, detail={
                            "code": err["code"], "err_msg": err["msg"]})

    if person.middle_name is None:
        return AddPersonCode200(data=f"{person.last_name} {person.first_name} has been added")
    else:
        return AddPersonCode200(data=f"{person.last_name} {person.first_name} {person.middle_name} has been added")


@router.get("/person", tags=["Person"])
@version(1)
async def get_person_id(person: GetIdPerson = Depends()) -> List[GetIdPersonCode200]:

    result: List[GetIdPersonCode200] = []
    logger.debug(person.middle_name)
    get_id_person: domain_model_person.GetIdPersonInput = domain_model_person.GetIdPersonInput(
        first_name=person.first_name,
        last_name=person.last_name,
        middle_name=person.middle_name
    )

    get_id_person_usecase: GetIdPersonUsecase = DomainContainer().get_id_person()

    data_persons, err = get_id_person_usecase.execute(get_id_person)

    if not err is None:
        raise HTTPException(status_code=400, detail={"err_msg": err})

    for data_person in data_persons:
        result.append(GetIdPersonCode200(
            id=data_person.id,
            firstName=data_person.first_name,
            lastName=data_person.last_name,
            middleName=data_person.middle_name,
            role=data_person.role,
            position=data_person.position
        ))

    return result


@router.put("/person", tags=["Person"], deprecated=True)
@version(1)
async def change_person() -> None:
    pass


@router.delete("/person", tags=["Person"], deprecated=True)
@version(1)
async def delete_person() -> None:
    pass
