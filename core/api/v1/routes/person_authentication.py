from pprint import pprint
from fastapi.exceptions import HTTPException
from fastapi import APIRouter, Body, Depends
from fastapi_versioning import version
from typing import List

from ...models.PersonAuthentication import PersonAuthentication, PersonAuthentication200

from core.di import DomainContainer
from core.domain.models.PersonAuthentication import PersonAuthentication as domain_model_person_auth
from core.domain.usecases import PersonAuthenticationUsecase

from loguru import logger


router: APIRouter = APIRouter()


@router.post("/auth", tags=["Authentication"])
@version(1)
async def person_authentication(person_authentication: PersonAuthentication = Depends()) -> PersonAuthentication200:

    # print(person_authentication.code.get_secret_value())
    person_auth_domain: domain_model_person_auth = domain_model_person_auth(
        code=str(person_authentication.code.get_secret_value()),
        telegram_id=person_authentication.telegram_id
    )

    person_auth: PersonAuthenticationUsecase = DomainContainer().person_authentication()

    result, err = person_auth.execute(person_authentication=person_auth_domain)

    if not err is None:
        raise HTTPException(status_code=400, detail={
                            "code": err["code"], "err_msg": err["msg"]})

    return PersonAuthentication200(
        data="The person is activated"
    )


@router.get("/auth", tags=["Authentication"], deprecated=True)
@version(1)
async def is_person_authentication() -> None:
    pass
