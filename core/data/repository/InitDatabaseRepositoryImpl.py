from core.domain.repository import IInitDatabaseTablesRepository
from ..storage.repository import IInitDatabaseTablesStorage


class IInitDBRepositoryImpl(IInitDatabaseTablesRepository):
    """
    Class
    ----------
    Repository implementation used to initialize database

    Implemented interfaces
    ----------
    IInitDBRepository

    Parameters
    ----------
    init_db_storage : IInitDBStorage
        Interface for initializing the database in the repository
    """

    def __init__(self, init_db_storage: IInitDatabaseTablesStorage) -> None:
        self.init_db_storage: IInitDatabaseTablesStorage = init_db_storage

    def init_tables(self) -> bool:
        return self.init_db_storage.init()
