from core.domain.repository import IPersonDatabaseRepository
from core.domain.models import Person as domain_model_person

from ..storage.repository.IPersonDatabaseStorage import IPersonDatabaseStorage
from ..storage.models import Person as storage_model_person


class PersonDatabaseRepositoryImpl(IPersonDatabaseRepository):
    """
    Class
    ----------


    Implemented interfaces
    ----------
    IPersonDatabaseRepository

    Parameters
    ----------

    """

    def __init__(self, person_database_storage: IPersonDatabaseStorage) -> None:
        self.person_database_storage: IPersonDatabaseStorage = person_database_storage

    def add_person(self, person: domain_model_person.AddPerson) -> tuple[bool, dict[str, int | str] | None]:
        person_storage: storage_model_person.AddPerson = storage_model_person.AddPerson(
            first_name=person.first_name,
            last_name=person.last_name,
            middle_name=person.middle_name,
            role=person.role,
            position=person.position
        )

        return self.person_database_storage.add_person(person=person_storage)

    def get_id_person(self, person: domain_model_person.GetIdPersonInput) -> tuple[list[domain_model_person.GetIdPersonOutput], None] | tuple[None, dict[str, int | str]]:
        get_id_person_storage: storage_model_person.GetIdPersonInput = storage_model_person.GetIdPersonInput(
            first_name=person.first_name,
            last_name=person.last_name,
            middle_name=person.middle_name
        )

        get_id_person_out_list, err = self.person_database_storage.get_id_person(
            person=get_id_person_storage)

        if err is None:
            result: list[domain_model_person.GetIdPersonOutput] = []

            for get_id_person_out in get_id_person_out_list:
                result.append(domain_model_person.GetIdPersonOutput(
                    id=get_id_person_out.id,
                    first_name=get_id_person_out.first_name,
                    last_name=get_id_person_out.last_name,
                    middle_name=get_id_person_out.middle_name,
                    role=get_id_person_out.role,
                    position=get_id_person_out.position
                ))

            return result, err
        else:
            return None, err

    def code_generate_for_sign_up(self, code_generate_person: domain_model_person.CodeGeneratePerson) -> tuple[bool, dict[str, int | str] | None]:
        code_generate_person_storage: storage_model_person.CodeGeneratePerson = storage_model_person.CodeGeneratePerson(
            person_id=code_generate_person.person_id,
            code=code_generate_person.code
        )

        return self.person_database_storage.add_code_for_sign_up(code_generage_person=code_generate_person_storage)
