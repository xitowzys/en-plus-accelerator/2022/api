from core.domain.repository import IAuthCodesDatabaseRepository
from core.domain.models.PersonAuthentication import PersonAuthentication

from ..storage.repository.IAuthCodesDatabaseStorage import IAuthCodesDatabaseStorage
from ..storage.models.PersonAuthentication import PersonAuthentication as storage_model_person_authentication


class AuthCodesDatabaseRepositoryImpl(IAuthCodesDatabaseRepository):

    def __init__(self, auth_codes_database_storage: IAuthCodesDatabaseStorage) -> None:
        self.auth_codes_database_storage: IAuthCodesDatabaseStorage = auth_codes_database_storage

    def person_authentication(self) -> bool:
        return True

    def exist_authentication_code(self, person_authentication: PersonAuthentication) -> tuple[bool, dict[str, int | str] | None]:
        person_authentication_storage: storage_model_person_authentication = storage_model_person_authentication(
            code=person_authentication.code,
            telegram_id=person_authentication.telegram_id
        )

        return self.auth_codes_database_storage.exist_authentication_code(
            person_authentication=person_authentication_storage
        )
