from pydantic.dataclasses import dataclass


@dataclass
class PersonAuthentication():
    code: str
    telegram_id: int
