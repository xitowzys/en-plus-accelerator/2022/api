from abc import ABC, abstractmethod
from typing import List, Union, Tuple
from ..models.Person import AddPerson, GetIdPersonInput, GetIdPersonOutput, CodeGeneratePerson


class IPersonDatabaseStorage(ABC):
    """
    Interface
    ----------
    The interface used to work with person

    Methods
    -------

    """

    @abstractmethod
    def add_person(self, person: AddPerson) -> tuple[bool, dict[str, int | str] | None]:
        pass

    @abstractmethod
    def get_id_person(self, person: GetIdPersonInput) -> tuple[list[GetIdPersonOutput], None] | tuple[None, dict[str, int | str]]:
        pass

    @abstractmethod
    def add_code_for_sign_up(self, code_generage_person: CodeGeneratePerson) -> tuple[bool, dict[str, int | str] | None]:
        pass
