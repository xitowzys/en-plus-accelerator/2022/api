from .IInitDatabaseTablesStorage import IInitDatabaseTablesStorage
from .IPersonDatabaseStorage import IPersonDatabaseStorage
from .IAuthCodesDatabaseStorage import IAuthCodesDatabaseStorage

__all__: list[str] = [
    "IInitDatabaseTablesStorage",
    "IPersonDatabaseStorage",
    "IAuthCodesDatabaseStorage"
]
