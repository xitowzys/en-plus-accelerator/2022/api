from loguru import logger
from .Errors import Errors

from ..repository.IAuthCodesDatabaseStorage import IAuthCodesDatabaseStorage
from ..models.PersonAuthentication import PersonAuthentication

from sqlalchemy.orm import sessionmaker
from .SQLAlchemy.engine import engine

from sqlalchemy.exc import NoResultFound

from .models.AuthCodes import AuthCodes as database_model_auth_codes
from .models.TelegramAccs import TelegramAccs
from typing import Any


class AuthCodes(IAuthCodesDatabaseStorage):
    def exist_authentication_code(self, person_authentication: PersonAuthentication) -> tuple[bool, dict[str, int | str] | None]:
        # logger.debug("AuthCodes (database)")
        err: dict[str, int | str] | None = None

        Session: sessionmaker = sessionmaker(bind=engine)
        session = Session()

        logger.debug(person_authentication.code)

        try:
            role_id: Any = session.query(database_model_auth_codes).filter(
                database_model_auth_codes.code == person_authentication.code).one()

            # logger.debug(role_id)

        except NoResultFound:
            logger.debug(Errors.auth_code_not_found())
            return False, Errors.auth_code_not_found()

        try:
            telegram_accs: TelegramAccs = TelegramAccs(
                telegram_id=person_authentication.telegram_id,
                person_id=role_id.person_id)

            role_id.is_used = 1
            session.add(telegram_accs)
            session.commit()
        except Exception as e:
            logger.debug(str(e))
            return False, Errors.any_error(msg=str(e))

        return True, None

    def person_authentication(self) -> bool:
        logger.debug("Person authentication (database)")
        return True
