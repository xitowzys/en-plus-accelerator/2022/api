import sqlalchemy as sa
from config import envs

engine = sa.create_engine(
    f"mariadb+mariadbconnector://{envs.ENVS['DB_USERNAME']}:{envs.ENVS['DB_PASSWORD']}@{envs.ENVS['DB_HOST']}:{envs.ENVS['DB_PORT']}/{envs.ENVS['DB_DATABASE']}")
