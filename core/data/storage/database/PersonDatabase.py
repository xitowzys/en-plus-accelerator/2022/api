from pprint import pprint
from typing import Any, Tuple, Union, List
from ..repository.IPersonDatabaseStorage import IPersonDatabaseStorage

from ..models.Person import AddPerson, GetIdPersonInput, GetIdPersonOutput, CodeGeneratePerson

from .models.Roles import Roles
from .models.Persons import Persons
from .models.Positions import Positions
from .models.AuthCodes import AuthCodes


from sqlalchemy.orm import sessionmaker
from .SQLAlchemy.engine import engine

from sqlalchemy.exc import NoResultFound, IntegrityError

from loguru import logger

from .Errors import Errors


class PersonDatabase(IPersonDatabaseStorage):
    """
    Class
    ----------
    The class used to ...

    Implemented interfaces
    ----------
    IInitDBStorage

    Methods
    -------
    init() : bool
        ...
    """

    def add_person(self, person: AddPerson) -> tuple[bool, dict[str, int | str] | None]:

        err: dict[str, int | str] | None = None

        Session: sessionmaker = sessionmaker(bind=engine)
        session = Session()

        try:
            role_id: Any = session.query(Roles).filter(
                Roles.title == person.role).one()

        except NoResultFound as e:
            return False, Errors.no_result_found(column="role")

        try:
            position_id: Any = session.query(Positions).filter(
                Positions.title == person.position).one()

        except NoResultFound as e:
            return False, Errors.no_result_found(column="position")

        try:
            persons: Persons = Persons(
                first_name=person.first_name,
                last_name=person.last_name,
                middle_name=person.middle_name,
                role_id=role_id.id,
                position_id=position_id.id
            )

            session.add(persons)
            session.commit()

            return True, err
        except Exception as e:
            return False, Errors.any_error(msg=str(e))

    def get_id_person(self, person: GetIdPersonInput) -> tuple[list[GetIdPersonOutput], None] | tuple[None, dict[str, int | str]]:

        result: List[GetIdPersonOutput] = []
        err: dict[str, int | str] | None = None

        Session: sessionmaker = sessionmaker(bind=engine)
        session = Session()

        logger.debug(person.middle_name)

        try:
            if person.middle_name is None:
                persons_output: Any = session.query(Persons).filter(
                    Persons.first_name == person.first_name, Persons.last_name == person.last_name, Persons.middle_name == None).all()
            else:
                persons_output: Any = session.query(Persons).filter(
                    Persons.first_name == person.first_name, Persons.last_name == person.last_name, Persons.middle_name == person.middle_name).all()

            for person_output in persons_output:
                get_id_person_output: GetIdPersonOutput = GetIdPersonOutput(
                    id=person_output._id,
                    first_name=person_output.first_name,
                    last_name=person_output.last_name,
                    middle_name=person_output.middle_name,
                    role=person_output.role.title,
                    position=person_output.positions.title
                )

                result.append(get_id_person_output)

            return result, err

        except Exception as e:
            return None, Errors.any_error(msg=str(e))

    def add_code_for_sign_up(self, code_generage_person: CodeGeneratePerson) -> tuple[bool, dict[str, int | str] | None]:
        err: dict[str, int | str] | None = None

        Session: sessionmaker = sessionmaker(bind=engine)
        session = Session()

        try:
            person: Any = session.query(Persons).filter(
                Persons._id == code_generage_person.person_id).one()

        except NoResultFound as e:
            return False, Errors.no_result_found(column="person")

        try:
            auth_code: AuthCodes = AuthCodes(
                code=code_generage_person.code,
                is_used=False,
                person_id=code_generage_person.person_id
            )

            session.add(auth_code)
            session.commit()

        except IntegrityError as e:
            session.rollback()
            print(Errors.code_exists(person=person.last_name))
            return False, Errors.code_exists(person=person.last_name)

        except Exception as e:
            return False, Errors.any_error(msg=str(e))

        return True, err
