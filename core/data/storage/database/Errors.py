
class Errors:

    @staticmethod
    def no_result_found(column: str) -> dict[str, int | str]:
        return {
            "code": 1,
            "msg": f"The \"{column}\" was not found"
        }

    @staticmethod
    def auth_code_not_found() -> dict[str, int | str]:
        return {
            "code": 3,
            "msg": f"The auth code was not found"
        }

    @staticmethod
    def any_error(msg: str) -> dict[str, int | str]:
        return {
            "code": -1,
            "msg": msg
        }

    @staticmethod
    def code_exists(person: str) -> dict[str, int | str]:
        return {
            "code": 2,
            "msg": f"The person {person} has a registration code"
        }
