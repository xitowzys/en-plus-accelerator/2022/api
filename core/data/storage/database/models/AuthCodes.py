from ..SQLAlchemy.declarative_base import Base

from sqlalchemy import INTEGER, Column, VARCHAR, BOOLEAN, ForeignKey

from dataclasses import dataclass


@dataclass
class AuthCodes(Base):
    __tablename__: str = 'auth_codes'

    id: Column = Column(INTEGER,
                        primary_key=True, autoincrement=True)
    code: Column = Column(VARCHAR(length=255), nullable=False, unique=True)
    is_used: Column = Column(BOOLEAN, nullable=False)
    person_id: Column = Column(INTEGER, ForeignKey("persons.id"), unique=True)
