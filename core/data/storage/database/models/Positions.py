from ..SQLAlchemy.declarative_base import Base

from sqlalchemy import INTEGER, Column, VARCHAR

from dataclasses import dataclass


@dataclass
class Positions(Base):
    __tablename__: str = 'positions'

    id: Column = Column(INTEGER,
                        primary_key=True, autoincrement=True)
    title: Column = Column(VARCHAR(length=255), nullable=False)
