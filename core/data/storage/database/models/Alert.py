from ..SQLAlchemy.declarative_base import Base

from sqlalchemy import INTEGER, TIMESTAMP, Column, TEXT, VARCHAR, BOOLEAN

from dataclasses import dataclass


@dataclass
class Alert(Base):
    __tablename__: str = 'alert'

    id: Column = Column(INTEGER,
                        primary_key=True, autoincrement=True)
    time: Column = Column(TIMESTAMP, nullable=False)
    message: Column = Column(TEXT, nullable=False)
    image: Column = Column(VARCHAR(length=255), nullable=False)

    is_sent: Column = Column(BOOLEAN(), nullable=False)
