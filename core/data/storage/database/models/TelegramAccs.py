from ..SQLAlchemy.declarative_base import Base

from sqlalchemy import INTEGER, Column, ForeignKey

from dataclasses import dataclass


@dataclass
class TelegramAccs(Base):
    __tablename__: str = 'telegram_accs'

    id: Column = Column(INTEGER,
                        primary_key=True, autoincrement=True)

    telegram_id: Column = Column(INTEGER, nullable=False, unique=True)
    person_id: Column = Column(INTEGER, ForeignKey("persons.id"), unique=True)
