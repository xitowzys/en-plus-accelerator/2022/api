from ..SQLAlchemy.declarative_base import Base

from sqlalchemy import INTEGER, Column, VARCHAR

from dataclasses import dataclass


@dataclass
class Roles(Base):
    __tablename__: str = 'roles'

    id: Column = Column(INTEGER,
                primary_key=True, autoincrement=True)
    title: Column = Column(VARCHAR(length=255), nullable=False)
