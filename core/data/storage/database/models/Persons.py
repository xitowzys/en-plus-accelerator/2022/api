from typing import Any
from ..SQLAlchemy.declarative_base import Base

from sqlalchemy import INTEGER, Column, VARCHAR, ForeignKey
from sqlalchemy.orm import relationship
from sqlalchemy.orm import backref

from dataclasses import dataclass


@dataclass
class Persons(Base):
    __tablename__: str = 'persons'

    _id: Column = Column("id", INTEGER,
                         primary_key=True, autoincrement=True)

    first_name: Column = Column(VARCHAR(length=60), nullable=False)
    last_name: Column = Column(VARCHAR(length=60), nullable=False)
    middle_name: Column = Column(VARCHAR(length=60), nullable=True)

    role_id: Column = Column(INTEGER, ForeignKey("roles.id"))
    position_id: Column = Column(INTEGER, ForeignKey("positions.id"))

    role: Any = relationship(
        "Roles", backref=backref("persons"))
    positions: Any = relationship(
        "Positions", backref=backref("persons"))

    auth_code: Any = relationship("AuthCodes", backref=backref("person"))
