from typing import Literal
import sqlalchemy as sa
from sqlalchemy import inspect
from loguru import logger as log

from ..repository.IInitDatabaseTablesStorage import IInitDatabaseTablesStorage

from .SQLAlchemy.declarative_base import Base
from .SQLAlchemy.engine import engine


class DatabaseInitTables(IInitDatabaseTablesStorage):
    """
    Class
    ----------
    The class used to ...

    Implemented interfaces
    ----------
    IInitDBStorage

    Methods
    -------
    init() : bool
        ...
    """

    def __import_tables(self) -> None:
        from .models.Alert import Alert
        from .models.AuthCodes import AuthCodes
        from .models.Positions import Positions
        from .models.Roles import Roles
        from .models.Persons import Persons
        from .models.TelegramAccs import TelegramAccs

    def init(self) -> bool:
        self.__import_tables()

        log.info(f"Проверка таблиц")

        is_all_tables_exist = True

        for table in Base.metadata.tables.keys():
            if not sa.inspect(engine).has_table(table):
                engine.execute('SET FOREIGN_KEY_CHECKS = 0;')

                is_all_tables_exist: bool = False

                log.warning(f"Таблица {table} не существует")

                Base.metadata.create_all(
                    engine, tables=[Base.metadata.tables[table]])
                log.success(f"Таблица {table} создана")

                engine.execute('SET FOREIGN_KEY_CHECKS = 1;')

        if is_all_tables_exist:
            log.success(f"Все таблицы уже проинициализированны")

        return True
