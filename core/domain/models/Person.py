from typing import Union
from pydantic.dataclasses import dataclass
from pydantic import Field


@dataclass
class GetIdPersonOutput:
    id: int = Field(...)
    first_name: str = Field(...)
    last_name: str = Field(...)
    middle_name: Union[str, None] = None
    role: str = Field(...)
    position: str = Field(...)


@dataclass
class GetIdPersonInput:
    first_name: str
    last_name: str
    middle_name: Union[str, None] = None


@dataclass
class AddPerson:
    first_name: str
    last_name: str
    role: str
    position: str
    middle_name: Union[str, None] = None


@dataclass
class CodeGeneratePerson():
    person_id: int
    code: str = ""
