from abc import ABC, abstractmethod

from ..models.Person import AddPerson, GetIdPersonInput, GetIdPersonOutput, CodeGeneratePerson


class IPersonDatabaseRepository(ABC):
    """
    Interface
    ----------
    The interface used to work with person

    Methods
    -------

    """

    @abstractmethod
    def add_person(self, person: AddPerson) -> tuple[bool, dict[str, int | str] | None]:
        pass

    @abstractmethod
    def get_id_person(self, person: GetIdPersonInput) -> tuple[list[GetIdPersonOutput], None] | tuple[None, dict[str, int | str]]:
        pass

    @abstractmethod
    def code_generate_for_sign_up(self, code_generate_person: CodeGeneratePerson) -> tuple[bool, dict[str, int | str] | None]:
        pass
