from .IInitDatabaseTablesRepository import IInitDatabaseTablesRepository
from .IPersonDatabaseRepository import IPersonDatabaseRepository
from .IAuthCodesDatabaseRepository import IAuthCodesDatabaseRepository

__all__: list[str] = [
    "IInitDatabaseTablesRepository",
    "IPersonDatabaseRepository",
    "IAuthCodesDatabaseRepository"
]
