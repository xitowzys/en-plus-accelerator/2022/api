from abc import ABC, abstractmethod

from ..models.PersonAuthentication import PersonAuthentication


class IAuthCodesDatabaseRepository(ABC):

    @abstractmethod
    def person_authentication(self) -> bool:
        pass

    @abstractmethod
    def exist_authentication_code(self, person_authentication: PersonAuthentication) -> tuple[bool, dict[str, int | str] | None]:
        pass
