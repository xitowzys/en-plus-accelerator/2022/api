import bcrypt
import shortuuid
from config import envs

from ..models import Person
from ..repository.IPersonDatabaseRepository import IPersonDatabaseRepository


class CodeGenerationForSignUpPersonUsecase():
    """
    Class
    ----------


    Parameters
    ----------


    Methods
    -------
    execute()
        Execute usecase
    """

    def __init__(self, person_database_repository: IPersonDatabaseRepository) -> None:
        self.person_database_repository: IPersonDatabaseRepository = person_database_repository

    def execute(self, code_generate_person: Person.CodeGeneratePerson, length_code: int = 10) -> tuple[str | None, dict[str, int | str] | None]:

        # TODO: Checking for password length 10 <= code <= 30

        code: str = shortuuid.ShortUUID().random(length=length_code)

        # salt: bytes = envs.ENVS["CODE_SIGN_UP_SALT"].encode('utf-8')
        # code_hash: bytes = bcrypt.hashpw(code.encode('utf-8'), salt)

        code_generate_person.code = code

        _, err = self.person_database_repository.code_generate_for_sign_up(
            code_generate_person=code_generate_person)

        if err is None:
            return code_generate_person.code, err
        else:
            return None, err
