from typing import Tuple, Union, List
from ..repository.IPersonDatabaseRepository import IPersonDatabaseRepository
from ..models.Person import GetIdPersonInput, GetIdPersonOutput


class GetIdPersonUsecase():
    """
    Class
    ----------


    Parameters
    ----------


    Methods
    -------
    execute()
        Execute usecase
    """

    def __init__(self, person_database_repository: IPersonDatabaseRepository) -> None:
        self.person_database_repository: IPersonDatabaseRepository = person_database_repository

    def execute(self, person: GetIdPersonInput) -> tuple[list[GetIdPersonOutput], None] | tuple[None, dict[str, int | str]]:
        return self.person_database_repository.get_id_person(person=person)
