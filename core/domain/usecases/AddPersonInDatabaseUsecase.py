from typing import Union
from ..repository.IPersonDatabaseRepository import IPersonDatabaseRepository
from ..models.Person import AddPerson


class AddPersonInDatabaseUsecase():
    """
    Class
    ----------


    Parameters
    ----------


    Methods
    -------
    execute()
        Execute usecase
    """

    def __init__(self, person_database_repository: IPersonDatabaseRepository) -> None:
        self.person_database_repository: IPersonDatabaseRepository = person_database_repository
        pass

    def execute(self, person: AddPerson) -> tuple[bool, dict[str, int | str] | None]:
        return self.person_database_repository.add_person(person=person)
