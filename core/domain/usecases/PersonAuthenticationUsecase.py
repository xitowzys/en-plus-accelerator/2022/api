from ..models.PersonAuthentication import PersonAuthentication
from ..repository.IAuthCodesDatabaseRepository import IAuthCodesDatabaseRepository

from loguru import logger

import bcrypt
from config import envs


class PersonAuthenticationUsecase():
    """
    Class
    ----------


    Parameters
    ----------  


    Methods
    -------
    execute()
        Execute usecase
    """

    def __init__(self, auth_codes_database_repository: IAuthCodesDatabaseRepository) -> None:
        self.auth_codes_database_repository: IAuthCodesDatabaseRepository = auth_codes_database_repository

    def execute(self, person_authentication: PersonAuthentication) -> tuple[bool, dict[str, int | str] | None]:
        logger.debug(person_authentication)

        # salt: bytes = envs.ENVS["CODE_SIGN_UP_SALT"].encode('utf-8')

        # code_hash: str = bcrypt.hashpw(
        #     person_authentication.code.encode("utf-8"), salt).decode("utf-8")

        # person_authentication.code = code_hash

        exist_auth_code, err = self.auth_codes_database_repository.exist_authentication_code(
            person_authentication=person_authentication
        )

        logger.debug(exist_auth_code)

        return True, err
