from loguru import logger as log

from ..repository.IInitDatabaseTablesRepository import IInitDatabaseTablesRepository


class InitDatabaseTablesUsecase():
    """
    Class
    ----------
    The usecase used to initialize tables in the database

    Parameters
    ----------
    init_db_repository : IInitDBRepository
        Interface for database initialization

    Methods
    -------
    execute()
        Execute usecase
    """

    def __init__(self, init_db_repository: IInitDatabaseTablesRepository) -> None:
        self.init_db_repository: IInitDatabaseTablesRepository = init_db_repository
        pass

    def execute(self) -> bool:
        log.info("Инициализация таблиц в базу данных")
        return self.init_db_repository.init_tables()
