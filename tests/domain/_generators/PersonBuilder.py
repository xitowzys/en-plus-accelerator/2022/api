from typing import Any
from typing_extensions import Self
from core.domain.models import Person
from faker import Faker
import random

roles: list[str] = [
    "Администратор",
    "Работник"
]

positions: list[str] = [
    "Сварщик",
    "Слесарь"
]


class AddPersonBuilder():
    def __init__(self) -> None:
        self.__result: dict[str, Any] = {}

    def set_first_name(self, first_name: str = Faker('ru_RU').first_name_male()) -> Self:
        self.__result["first_name"] = first_name
        return self

    def set_last_name(self, last_name: str = Faker('ru_RU').last_name_male()) -> Self:
        self.__result["last_name"] = last_name
        return self

    def set_middle_name(self, middle_name: str = Faker('ru_RU').middle_name_male()) -> Self:
        self.__result["middle_name"] = middle_name
        return self

    def set_role(self, role: str = random.choices(roles, weights=[1, 3])[0]) -> Self:
        self.__result["role"] = role
        return self

    def set_position(self, position: str = random.choice(positions)) -> Self:
        self.__result["position"] = position
        return self

    @property
    def build_random(self) -> Person.AddPerson:
        self.set_first_name()
        self.set_last_name()
        self.set_middle_name()
        self.set_role()
        self.set_position()

        return self.build

    @property
    def build(self) -> Person.AddPerson:
        return Person.AddPerson(
            first_name=self.__result["first_name"],
            last_name=self.__result["last_name"],
            role=self.__result["role"],
            position=self.__result["position"],
            middle_name=self.__result["middle_name"]
        )
