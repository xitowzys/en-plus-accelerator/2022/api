# def test_example():
#     print("test")

from core.domain.usecases import AddPersonInDatabaseUsecase
from core.domain.models import Person
from core.domain.repository import IPersonDatabaseRepository
import pytest
import random

from tests.domain._generators import PersonBuilder
# from core.data.repository import PersonDatabaseRepositoryImpl
# from abc import ABC, abstractmethod


# class IPersonDatabaseRepository(ABC):

#     @abstractmethod
#     def add_person(self, person: Person.AddPerson) -> tuple[bool, dict[str, int | str] | None]:
#         pass


class PersonDatabaseRepositoryImpl(IPersonDatabaseRepository):

    def __init__(self):
        print("OK")

    def add_person(self, person: Person.AddPerson) -> tuple[bool, dict[str, int | str] | None]:

        results = [(True, None), (False, {"code": 0, "msg": "str"})]

        return random.choice(results)

    def get_id_person(self, person: Person.GetIdPersonInput) -> tuple[list[Person.GetIdPersonOutput], None] | tuple[None, dict[str, int | str]]:

        test_person = [Person.GetIdPersonOutput(
            id=0,
            first_name="test",
            last_name="test",
            role="test",
            position="test"
        )]

        return test_person, None

    def code_generate_for_sign_up(self, code_generate_person: Person.CodeGeneratePerson) -> tuple[bool, dict[str, int | str] | None]:
        return True, None


# @pytest.mark.domain_usecases
def test_add_person_in_database_usecase_test():
    add_person_in_database_usecase = AddPersonInDatabaseUsecase(
        person_database_repository=PersonDatabaseRepositoryImpl())

    for _ in range(10):
        person: Person.AddPerson = PersonBuilder.AddPersonBuilder().build_random

        result: tuple[bool, dict[str, int | str] |
                      None] = add_person_in_database_usecase.execute(person=person)

        assert (True, None) == result or (
            False, {"code": 0, "msg": "str"}) == result


def test_example():
    print(PersonBuilder.AddPersonBuilder().build_random)
